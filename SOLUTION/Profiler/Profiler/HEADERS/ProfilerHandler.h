#pragma once
#include "Profiler.h"
#include "GLFW/glfw3.h"

class ProfilerHandler
{
public:

	//IF YOU DONT HAVE IMGUI CALL THESE FUNCTIONS IN ORDER:
	void initialize_window();
	void initialize_imgui();

	void imgui_preupdate();
	void imgui_render();


	//IF YOU ALREADY HAVE IMGUI, CALL THESE MAKING SURE YOU ARE RENDERING IMGUI AFTERWARDS AND THAT YOU ARE STARTNG THE IMGUI FRAME
	void imgui_profiler();

private:
	void r_imgui_tree(profiler::call_node * current);

public:

	GLFWwindow * m_window;
private:
	profiler::call_node * master_node = nullptr;

	int m_each_frame = 5;

	bool m_activate = false;
	bool m_get_files = false;


};