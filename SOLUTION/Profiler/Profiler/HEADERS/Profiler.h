#pragma once
#include <iostream>
#include <vector>
#include "windows.h"


namespace profiler
{
	struct raw_info
	{
		unsigned n_calls{ 0 };
		unsigned n_recursive{ 0 };
		unsigned n_recursive_counter{ 0 };

		unsigned long long n_min_time{ unsigned long long(-1) };
		unsigned n_min_fn{ 0 };

		unsigned long long n_max_time{ 0u };
		unsigned n_max_fn{ 0 };

		unsigned long long n_latest_time{0u};
		unsigned n_cur_fn{ 0 };

		unsigned long long n_total_time{ 0u };
	};

	struct call_node
	{
		const char * n_stage_name;

		std::vector<call_node *> n_children;
		call_node * n_parent{ nullptr };

		//Stats
		raw_info n_information;
	};

	//To inform where the frame starts to the profiler
	void profiler_start_frame();

	//The start of a profiling stage
	void profiler_start_stage(const char * name);

	//To show where it ends
	void profiler_end_stage();

	//Funcition to start profiling
	void profile(bool profile);

	//Funcition to clear the info in real time
	void clear_info();

	//Function to get the frame count
	unsigned get_current_frame();

	//Get the information treated to the console or file per frame you wish to have.
	void get_treated_info(bool console, bool file, unsigned each_frame = 0);

	//Get the information and treat it yourself.
	call_node * get_raw_info();
}

