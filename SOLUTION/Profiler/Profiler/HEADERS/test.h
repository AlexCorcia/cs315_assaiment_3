#pragma once
#include "Profiler.h"
#include "ProfilerHandler.h"

void test_recursive(int a);

int test_triple_for_loop (unsigned a);

float test_long_addition_per_step(float a, unsigned i_max, float b);

void imgui_test(ProfilerHandler * handler);

void console_test();

