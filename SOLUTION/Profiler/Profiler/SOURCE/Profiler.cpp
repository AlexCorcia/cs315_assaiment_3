#include "Profiler.h"
#include <iostream>
#include <fstream>
#include <string>
#include <direct.h>

//The actual implementation of the profiler
namespace profiler
{
	class Profiler
	{
	public: // "public"
		void create_node(const char * node_name);
		void close_node(unsigned long long time_to_close);

		std::string information_dump();
		
		void clear_all();


		~Profiler() { clear_all(); }

	private: // private functions

		void information_dump_r(std::string & c_string, call_node * c_node, unsigned depth = 0);
		void clear_all_r(call_node * c_node);

	public: // "public" variables
		call_node * m_root{ nullptr };
		call_node * m_current{ nullptr };
		call_node * m_previous{ nullptr };

		unsigned m_current_frame{ 0 };

		bool m_profile = false;
		bool m_frame_started = false;
		
	private: // private variables
	};

	Profiler master;

	void Profiler::create_node(const char *  node_name)
	{
		//The root is a node with no info inside of it, it is just the starting point of the tree
		//Because the tree can have two starting functions and that could make two roots.
		if (m_root == nullptr)
		{
			//Starting the tree so we do the warm up
			__rdtsc();
			__rdtsc();
			__rdtsc();

			m_root = new call_node;
			m_current = m_root;
			m_root->n_stage_name = "ROOT";
			m_previous = nullptr;
		}


		if (m_current->n_stage_name == node_name)
		{
			m_current->n_information.n_recursive++;
			m_current->n_information.n_recursive_counter++;
			return;
		}


		//Going through all child to check if we already opened and advnace if opened
		for (unsigned i = 0; i < m_current->n_children.size(); i++)
		{
			if (m_current->n_children[i]->n_stage_name == node_name)
			{
				m_previous = m_current;
				m_current = m_current->n_children[i];

				//Checking if we are in a recursive case
				// or did we are reopening the tree
				if (m_previous == m_current)
					m_current->n_information.n_recursive++;
				else
				{
					m_current->n_information.n_calls++;
					m_current->n_information.n_cur_fn++;
					m_current->n_information.n_latest_time = __rdtsc();
				}

				return;
			}
		}

		//GENERATING A NEW NODE, SHOULD ONLY HAPPEND THE FIRST TIME OR IF WE GO ON A NEW BRANCH OF THE CODE
		/**************************************************************************************************/
		//Creating the node and setting all of the information of it
		call_node * new_node = new call_node;
		new_node->n_stage_name = node_name;
		new_node->n_parent = m_current;

		new_node->n_information.n_cur_fn = m_current_frame;
		new_node->n_information.n_calls++;

		//Moving the current if needed.
		m_current->n_children.push_back(new_node);
		m_previous = m_current;
		m_current = new_node;
		m_current->n_information.n_latest_time = __rdtsc();
		/**************************************************************************************************/
	}

	void Profiler::close_node(unsigned long long time)
	{
		//Closing down a node but letting it there
		//Getting the time

		if (m_current->n_information.n_recursive_counter > 0)
		{
			m_current->n_information.n_recursive_counter--;
			return;
		}

		m_current->n_information.n_latest_time = time - m_current->n_information.n_latest_time;
		m_current->n_information.n_total_time += m_current->n_information.n_latest_time;

		//Checking if it this greater or less than what we had before.
		if (m_current->n_information.n_latest_time > m_current->n_information.n_max_time)
		{
			m_current->n_information.n_max_time = m_current->n_information.n_latest_time;
			m_current->n_information.n_max_fn = m_current->n_information.n_cur_fn;
		}

		if (m_current->n_information.n_latest_time < m_current->n_information.n_min_time)
		{
			m_current->n_information.n_min_time = m_current->n_information.n_latest_time;
			m_current->n_information.n_min_fn = m_current->n_information.n_cur_fn;
		}

		m_current = m_current->n_parent;
	}

	std::string Profiler::information_dump()
	{
		std::string new_info;
		new_info += "****************************************************************\n";
		new_info += "Profiler Information Drop: \n";
		new_info += "Frame #" + std::to_string(m_current_frame);
		new_info += "\n";


		information_dump_r(new_info, m_root);


		new_info += "****************************************************************\n";

		return new_info;
	}

	void Profiler::information_dump_r(std::string & c_string, call_node * c_node, unsigned depth)
	{
		std::string tabs;

		if (c_node == nullptr)
			return;

		for (unsigned i = 0; i < depth; i++)
			tabs += "\t";

		if (c_node != m_root)
		{
			c_string += tabs +  "Stage Name: ";
			c_string += c_node->n_stage_name;
			c_string += "\n";

			c_string += tabs + "Calls: " + std::to_string(c_node->n_information.n_calls) + "\n";
			c_string += tabs + "Recusive calls: " + std::to_string(c_node->n_information.n_recursive) + "\n";

			float t_percentage = 100.0f;
			float c_percentage = 100.0f;

			if (c_node->n_parent != m_root)
			{
				t_percentage = (float(c_node->n_information.n_total_time) / float(c_node->n_parent->n_information.n_total_time)) * 100.0f;
				c_percentage = std::round((float(c_node->n_information.n_latest_time) / float(c_node->n_parent->n_information.n_latest_time)) * 100.0f);
			}

			c_string += tabs + "Total Percentage: " + std::to_string(t_percentage) + "% \n";
			c_string += tabs + "Current Percentage: " + std::to_string(c_percentage) + "% \n";

			c_string += tabs + "Cycles: " + std::to_string(c_node->n_information.n_latest_time) + "\n";

			c_string += tabs + "Max Cycles: " + std::to_string(c_node->n_information.n_max_time) + "[" + std::to_string(c_node->n_information.n_max_fn) + "]" + "\n";

			c_string += tabs + "Min Cycles: " + std::to_string(c_node->n_information.n_min_time) + "[" + std::to_string(c_node->n_information.n_min_fn) + "]" + "\n";

			c_string += "\n\n";
		}
		else
			c_string += "START \n";


		if (c_node->n_children.empty())
			return;

		for (unsigned i = 0; i < c_node->n_children.size(); i++)
			information_dump_r(c_string, c_node->n_children[i], depth+1);
	}

	void Profiler::clear_all()
	{
		clear_all_r(m_root);
		m_root = nullptr;
	}

	void Profiler::clear_all_r(call_node * c_node)
	{
		if (c_node == nullptr)
			return;

		unsigned size = c_node->n_children.size();

		for (unsigned i = 0; i < size; i++)
			clear_all_r(c_node->n_children[i]);

		delete c_node;
	}
}

//The code accessible by the "public"
namespace profiler
{
	void profiler_start_frame()
	{
		if (master.m_profile == true)
			master.m_frame_started = true;
		else
			master.m_frame_started = false;

		master.m_current_frame++;
	}

	void profiler_start_stage(const char * name)
	{
		if(master.m_frame_started == true)
			master.create_node(name);
	}

	void profiler_end_stage()
	{
		if (master.m_frame_started == true)
			master.close_node(__rdtsc());
	}


	/*UTILITIES*/

	void profile(bool profile)
	{
		master.m_profile = profile;
	}

	void clear_info()
	{
		master.clear_all();
	}

	unsigned get_current_frame()
	{
		return master.m_current_frame;;
	}

	void get_treated_info(bool console, bool file, unsigned each_frame)
	{
		if (!master.m_profile)
			return;

		if (each_frame != 0)
		{
			if (master.m_current_frame % each_frame != 0)
				return;
		}
		if (!console && !file)
			return;

		
		std::string new_text = master.information_dump();

		if(console)
		{
			HANDLE stdoutHandler;
			CONSOLE_SCREEN_BUFFER_INFO csbi;

			stdoutHandler = GetStdHandle(STD_OUTPUT_HANDLE);

			GetConsoleScreenBufferInfo(stdoutHandler, &csbi);

			csbi.dwCursorPosition.X = 0;	//Reset the position of the console cursor to the beginning
			csbi.dwCursorPosition.Y = 0;

			SetConsoleCursorPosition(stdoutHandler, csbi.dwCursorPosition);

			std::cout << new_text << std::endl;
		}
		if (file)
		{
			_mkdir("PROFILER_INFO");
			std::string position = "PROFILER_INFO/";
			std::string filename = "pro_frame_" + std::to_string(master.m_current_frame) + ".txt" ;
			std::ofstream myfile;
			myfile.open(position + filename);

			myfile << new_text;
			myfile.close();
		}

	}

	call_node * get_raw_info()
	{
		return master.m_root;
	}

}

