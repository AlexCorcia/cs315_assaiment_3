#include "test.h"

void test_recursive(int a)
{
	profiler::profiler_start_stage("RECURSIVE");

	if (a == 0)
	{
		profiler::profiler_end_stage();
		return;
	}

	test_recursive(--a);

	profiler::profiler_end_stage();
}
int test_triple_for_loop(unsigned a)
{
	profiler::profiler_start_stage("TRIPLE FOR LOOP");

	int counter = 0;

	for (unsigned i = 0; i < a; i++)
	{
		for (unsigned j = 0; j < a; j++)
		{
			for (unsigned k = 0; k < a; k++) 
			{
				counter++; 
			}

		}
	}
				

	profiler::profiler_end_stage();
	return counter;
}

float test_long_addition_per_step(float a, unsigned i_max, float b)
{
	profiler::profiler_start_stage("LONG ADDITION");

	float result = 0;

	for (unsigned i = 0; i < i_max; i++)
	{
		result += b;
	}

	profiler::profiler_end_stage();
	return result;
}

void imgui_test(ProfilerHandler * handler)
{
	while (!glfwWindowShouldClose(handler->m_window))
	{
		glfwSwapBuffers(handler->m_window);

		handler->imgui_preupdate();
		
		profiler::profiler_start_frame();
		profiler::profiler_start_stage("MAIN");

		int recursion_test = 300;
		unsigned triple_for_loop_test = 25;

		float a = 25.0f;
		float b = 500.5f;
		unsigned i_max = 10000;

		test_recursive(recursion_test);
		float test_c = test_long_addition_per_step(a, i_max, b);
		float test_b = test_triple_for_loop(triple_for_loop_test);

		profiler::profiler_end_stage();

		handler->imgui_profiler();
		handler->imgui_render();

		glfwPollEvents();

		if (glfwGetKey(handler->m_window, GLFW_KEY_Q) && glfwGetKey(handler->m_window, GLFW_KEY_LEFT_CONTROL))
			glfwSetWindowShouldClose(handler->m_window, GLFW_TRUE);
	}
}

void console_test()
{

}