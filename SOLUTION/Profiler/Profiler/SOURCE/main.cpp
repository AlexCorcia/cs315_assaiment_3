#include "Profiler.h"
#include "ProfilerHandler.h"
#include "test.h"

#define USING_IMGUI

#ifdef USING_IMGUI

	void test()
	{
		ProfilerHandler * profiler_handle = new ProfilerHandler();

		profiler_handle->initialize_window();
		profiler_handle->initialize_imgui();

		imgui_test(profiler_handle);
	}

#else

	void test()
	{
		profiler::profile(true);

		
		profiler::profiler_start_frame();
		profiler::profiler_start_stage("MAIN");

		int recursion_test = 300;
		unsigned triple_for_loop_test = 25;

		float a = 25.0f;
		float b = 500.5f;
		unsigned i_max = 10000;

		test_recursive(recursion_test);
		float test_c = test_long_addition_per_step(a, i_max, b);
		float test_b = test_triple_for_loop(triple_for_loop_test);

		profiler::profiler_end_stage();
		

		profiler::get_treated_info(true, false);

		system("pause");
	}
	
#endif // USING_IMGUI

int main()
{
	//This function call will be diferent depending on the define of USING_IMGUI
	test();
}