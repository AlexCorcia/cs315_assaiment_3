#include "ProfilerHandler.h"

#include "glad/glad.h"

#include <iostream>
#include <string>

#include "imgui/imgui.h"
#include "imgui/imgui_impl_glfw.h"
#include "imgui/imgui_impl_opengl3.h"



#define WINDOW_WIDTH 1920
#define WINDOW_HEIGHT 1080

void framebuffer_size_callback(GLFWwindow* window, int width, int height) {}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	(void)mods;
	(void)scancode;
	(void)key;
	(void)action;
}

void ProfilerHandler::initialize_window()
{
	glfwInit();

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	m_window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "CS315 PLACEHOLDER", NULL, NULL);

	if (m_window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return;
	}

	glfwMakeContextCurrent(m_window);
	glfwSetFramebufferSizeCallback(m_window, framebuffer_size_callback);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return;
	}


	//Setting the Key CallBack
	glfwSetKeyCallback(m_window, key_callback);
	glfwMakeContextCurrent(m_window);

	glfwSwapInterval(1);
}

void ProfilerHandler::initialize_imgui()
{
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO();
	(void)io;

	ImGui::StyleColorsDark();

	//Setup Platform/Renderer bindings
	const char* glsl_version = "#version 130";
	ImGui_ImplGlfw_InitForOpenGL(m_window, true);
	ImGui_ImplOpenGL3_Init(glsl_version);

}

void ProfilerHandler::imgui_preupdate()
{
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();
}

void ProfilerHandler::imgui_render()
{
	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

void ProfilerHandler::imgui_profiler()
{
	ImGui::StyleColorsDark();
	{
		ImGui::Begin("Profiler Handeler");

		std::string c_string = "Current Frame: " + std::to_string(profiler::get_current_frame()) + "\n";
		ImGui::Text(c_string.c_str());

		ImGui::Separator();
		ImGui::Checkbox("Activate Profiler", &m_activate);
		profiler::profile(m_activate);



		ImGui::Separator();
		ImGui::Text("File Keeping");
		ImGui::InputInt("File Frame Keeping", &m_each_frame);
		ImGui::Checkbox("Keep Information", &m_get_files);
		if(m_get_files)
			profiler::get_treated_info(false,true,m_each_frame);

		ImGui::Separator();

		if(ImGui::Button("Clear Information"))
			profiler::clear_info();
		ImGui::Separator();

		ImGui::Text("PROFILER");
		master_node = profiler::get_raw_info();

		if (master_node != nullptr)
		{
			r_imgui_tree(master_node->n_children[0]);
		}
		
		ImGui::End();
	}
}

void ProfilerHandler::r_imgui_tree(profiler::call_node * current)
{
	if (ImGui::TreeNode(current->n_stage_name))
	{
		std::string c_string = "Calls: " + std::to_string(current->n_information.n_calls) + "\n";
		ImGui::TextColored(ImVec4(1.0f, 1.0f, 1.0f, 1.0f), c_string.c_str());

		c_string = "Recusive calls: " + std::to_string(current->n_information.n_recursive) + "\n";
		ImGui::TextColored(ImVec4(0.5f, 0.5f, 0.5f, 1.0f), c_string.c_str());

		float t_percentage = 100.0f;
		float c_percentage = 100.0f;

		if (current->n_parent != master_node)
		{
			t_percentage = (float(current->n_information.n_total_time) / float(current->n_parent->n_information.n_total_time)) * 100.0f;
			c_percentage = std::round((float(current->n_information.n_latest_time) / float(current->n_parent->n_information.n_latest_time)) * 100.0f);
		}

		c_string ="Total Percentage: " + std::to_string(t_percentage) + "% \n";
		ImGui::TextColored(ImVec4(0.0f, 1.0f, 1.0f, 1.0f), c_string.c_str());
		c_string ="Current Percentage: " + std::to_string(c_percentage) + "% \n";
		ImGui::TextColored(ImVec4(0.0f, 0.6f, 0.6f, 1.0f), c_string.c_str());

		c_string ="Cycles: " + std::to_string(current->n_information.n_latest_time) + "\n";
		ImGui::TextColored(ImVec4(0.0f, 0.0f, 1.0f, 1.0f), c_string.c_str());

		c_string ="Max Cycles: " + std::to_string(current->n_information.n_max_time) + "[" + std::to_string(current->n_information.n_max_fn) + "]" + "\n";
		ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), c_string.c_str());

		c_string ="Min Cycles: " + std::to_string(current->n_information.n_min_time) + "[" + std::to_string(current->n_information.n_min_fn) + "]" + "\n";
		ImGui::TextColored(ImVec4(0.0f, 1.0f, 0.0f, 1.0f), c_string.c_str());


		unsigned size = current->n_children.size();

		for (unsigned i = 0; i < size; i++)
		{
			r_imgui_tree(current->n_children[i]);

		}
		ImGui::TreePop();
	}
}