How was the overall experience with the assignment?
I really enjoyed this assignment, plus because of the changes I had to prepare a solution with ImGui and prepare everything for it to work so
at least it was entretaining.

How long did the assignment take?
The changes were bigger than expected I did it in a couple of days.
I also noticed I had a bug with the recursion algorithms and I had to rework on that.

What would you say you took away from doing it? In other words, what did you learn from it?
I mostly knew already how the assignment work (apart from that bug).

What was the hardest part about it?
Adding Imgui.

Is there anything in particular you would change (to improve the assignment experience as a learning device)?
The handout was a little bit unclear about the changes from this year to the previous one.

Additional comments or suggestions.